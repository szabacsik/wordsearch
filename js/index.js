$(document).ready(function(){

    var alpacaForm = $("#alpacaform").alpaca({
        "dataSource": "./src/form_data.json",
        "optionsSource": "./src/form_options.json",
        "schemaSource": "./src/form_schema.json",
        "view": {
            "parent": "bootstrap-create"
        },
        "options": {
            "form": {
                "attributes": {
                    "method": "post",
                    "action": "http://szabacsik.com/wordsearch/worker.php"
                },
                "buttons": {
                    "submit": {
                        "click": function(e) {
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                            modal.style.display = "block";
                            start_flash();
                            var promise = this.ajaxSubmit();
                            promise.done(function() {
                                worker_feedback ( promise.responseJSON );
                            });
                            promise.fail(function() {
                                alert("Something went wrong during the transaction");
                            });
                            promise.always(function() {
                                //alert("Completed");
                                modal.style.display = "none";
                                stop_flash();
                            });
                        }
                    }
                }
            },
            "fields": {
                "board": {
                    "fields": {
                        "source": {
                            "events": {
                                "change": function() {
                                    board_render ( this.getValue() );
                                }
                            }
                        }
                    }

                }
            }
        }//,
/*
        "postRender": function(control) {
            $( ".alpaca-form-button" ).click(function() {
                var value = control.getValue();
                alert(JSON.stringify(value, null, "  "));
            });
        }
*/
    });

    function worker_feedback ( messages )
    {
        $('.messages').find("p").remove();
        $('.messages').find("h2").after( "<p>"+messages.options+"</p><p>"+messages.wordsearch+"</p>" );
        solutions_render ( messages.solutions );
    }

    function board_render ( source )
    {
        var view = '<div class="rendered-board" style="width:100%;">';
        $.post ( "worker.php", { task: "render", source: source } )
            .done(function( data )
            {
                var board = JSON.parse(data);
                var matrix = board.matrix;
                var dimensions = board.dimensions;
                dimensions.x = parseInt ( dimensions.x ) + 1;
                var width_percent = Math.floor ( 100 / dimensions.x );
                for (var key in matrix)
                {
                    var coordinate = key.split(',');
                    var letter = matrix [ key ];
                    ////console.log("on coordinate [" + coordinate[0] + "," + coordinate[1] + "] the letter is " + letter );
                    view += '<div class="rendered-letter coordinate-'+coordinate[0] + '-' + coordinate[1] + ' letter-' + letter + '" style="width:'+width_percent+'%;"><span>'+letter+'</span></div>';
                }
                view += '</div>';
                $('.board').find(".rendered-board").first().remove();
                $('.board').find("h2").first().after( view );
                fix_letter_height();
            });
        //todo: error handling
    }

    function fix_letter_height ()
    {
        var width = $('.rendered-letter:first').width();
        var last_classes = $('.rendered-letter').last().attr('class').split(/\s+/);
        var last_coordinates = last_classes[1];
        last_coordinates = last_coordinates.split("-");
        var last_x = parseInt(last_coordinates [1]);
        var last_y = parseInt(last_coordinates [2]);
        var title_height = parseInt($('.board-inside').find('h2').first().height());
        //var padding = 10;
        //console.log(last_y);
        $('.rendered-letter').height(width);
        $('.rendered-letter').find('span').css({'font-size': (width/2) + 'px', 'line-height': width + 'px'});
        $('.rendered-board').height((((last_y+1)*width)*1.09)+title_height);
        //$('.board-inside').height(700);
    }

    function convert_path ( path )
    {
        var result = '';
        for ( var letter_index in path )
        {
            var letter_data = path [ letter_index ];
            var x = letter_data.x;
            var y = letter_data.y;
            //console.log(letter_index+': '+JSON.stringify(letter_data));
            //console.log('coordinates: '+x+';'+y);
            result += x+';'+y+'#';
        }
        return result.slice(0, -1);
    }

    function solutions_render ( solutions_string )
    {
        var solutions = JSON.parse(solutions_string);
        var view = '<div class="solutions-rendered">';
        for (var key in solutions)
        {
            var word_id = "word-"+key.toString();
            var solution = solutions [ key ];
            //console.log(key+': '+JSON.stringify(solution.path));
            view += '<div class="word-rendered" id="'+word_id+'" data-path="'+convert_path ( solution.path )+'">'+solution.word+'</div>';
            //$('#'+word_id).data('solution_path',JSON.stringify(solution.path));
            //$('.'+word_id).css({'color':'yellow'});
            //break;
        }
        view += '</div>';
        $('.results').find(".solutions-rendered").first().remove();
        $('.results').find("h2").after( view );

        $('.word-rendered')
            .mouseover(function()
            {
                //console.log($(this).attr('id') + ' over');
                var path = $(this).data('path');
                path = path.split('#');
                //console.log(path);
                for (var key in path)
                {
                    change_design ( path[key], 'add', 'active-letter' );
                }
                var spell = $(this).text().split('');
                var say = $(this).text();
                var msg = new SpeechSynthesisUtterance(spell+'. '+say);
                window.speechSynthesis.speak(msg);
            })
            .mouseout(function()
            {
                //console.log($(this).attr('id') + ' over');
                var path = $(this).data('path');
                path = path.split('#');
                //console.log(path);
                for (var key in path)
                {
                    change_design ( path[key], 'remove', 'active-letter' );
                }
                window.speechSynthesis.cancel();
            });

    }

    board_render ( 'regular' );

    var modal = document.getElementById('myModal');

    var timer;

    function start_flash ()
    {

        //console.log('start flash');

        function randRange ( data )
        {
            var newTime = data[Math.floor(data.length * Math.random())];
            return newTime;
        }

        function toggle_flashing ()
        {
            var timeArray = new Array(5, 10, 15, 20, 25, 50);

            // do stuff, happens to use jQuery here (nothing else does)
            var x = Math.floor((Math.random() * 11));
            var y = Math.floor((Math.random() * 11));
            change_design ( x.toString()+';'+y.toString(), 'switch', 'flash-letter' );
            //$("#box").toggleClass("visible");

            clearInterval(timer);
            timer = setInterval(toggle_flashing, randRange(timeArray));
        }

        timer = setInterval(toggle_flashing, 5);
    }

    function stop_flash ()
    {
        clearInterval(timer);
        for (x = 0; x < 10; x++)
        {
            for (y = 0; y < 10; y++)
            {
                change_design ( x.toString()+';'+y.toString(), 'remove', 'flash-letter' );
            }
        }
    }

    function change_design ( coordinates, operation, classname )
    {
        if ( Array.isArray ( coordinates ) )
        {
            var x = coordinates [ 0 ];
            var y = coordinates [ 1 ];
        }
        else
        {
            const regex = /[\s,;]+/;
            var _coordinates = coordinates.split(regex);
            var x = _coordinates [ 0 ];
            var y = _coordinates [ 1 ];
        }
        var coordinate_class = '.coordinate-'+x.toString()+'-'+y.toString();
        switch ( operation )
        {
            case 'add' :
                $(coordinate_class).addClass( classname );
            break;
            case 'remove' :
                $(coordinate_class).removeClass( classname );
            break;
            case 'switch' :
                $(coordinate_class).toggleClass( classname );
            break;
        }

    }

    function reset_design ()
    {

    }

    $( window ).resize(function() {
        //console.log('resize');
        fix_letter_height ();
    });


});

