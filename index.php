<?php

require_once ( 'inc/constants.php');
require_once ( 'inc/functions.php');
require_once ( 'inc/time.class.php');
require_once ( 'inc/temphplate.class.php');

$page_content = json_decode ( file_get_contents ( dirname ( __FILE__ ) . '/src/index.json' ) );
$page_content -> messages = '';
$template = new template ( dirname ( __FILE__ ) . '/tpl/index.html', $page_content, 'variable', 'file' );
$template -> render ();
