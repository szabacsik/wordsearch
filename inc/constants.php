<?php
define ( 'windows_line_break', "\r\n" );
define ( 'linux_line_break', "\n" );
define ( 'letter_separator', " " );
define ( 'output_line_break', "<br>" );
define ( 'coordinate_separator', "," );
