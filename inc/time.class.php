<?php

class time
{
    public function __construct ()
    {
    }

    public function __destruct ()
    {
    }

    public function now ( $format = "Y-m-d H:i:s.u" )
    {
        $now = microtime ( true );
        $micro = sprintf ( "%06d", ( $now - floor ( $now ) ) * 1000000 );
        $date = new DateTime ( date ( 'Y-m-d H:i:s.' . $micro, $now ) );
        return $date -> format ( $format );
    }

    public function micro ( $get_as_float = true )
    {
        return microtime ( $get_as_float );
    }
}
