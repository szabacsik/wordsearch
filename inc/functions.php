<?php

//TODO: Error handling both textfile_convert and wordsearch class

function textfile_convert ( $textfile_content, $target = 'variable', $line_break = PHP_EOL, $letter_separator = " " )
{
    $result = array ();
    $lines = explode ( $line_break, $textfile_content );
    foreach ( $lines as $line_index => $line )
    {
        $letters = explode ( $letter_separator, $line );
        if ( count ( $letters ) > 0 )
        {
            foreach ( $letters as $letter_index => $letter )
            {
                $result [ $letter_index . coordinate_separator . $line_index ] = trim ( $letter );
            }
        }
        else break;
    }
    return $result;
}

class dictionary
{

    private $mode;
    private $data;
    public $longest;
    public $shortest;

    public function __construct ( $filepath, $mode = 'string' ) //file, array, string
    {
        $this -> mode = $mode;
        switch ( $this -> mode )
        {
            case 'string':
            default:
                $this -> data = file_get_contents ( $filepath );
            break;
            case 'array':
                $this -> data = array_map ( 'trim', explode ( PHP_EOL, file_get_contents ( $filepath ) ) );
            break;
            case 'file':
                $this -> data = $filepath;
            break;
        }
        $this -> longest = $this -> get_longest ();
        $this -> shortest = $this -> get_shortest ();
        //file_put_contents('debug.log',$this->get_longest().' / '.$this->get_shortest());
        //var_export ( $this -> data );
    }

    public function partial ( $word )
    {
    }

    public function gotcha ( $word )
    {
        switch ( $this -> mode )
        {
            case 'string':
            default:
                //$re = '/^'.$word.'.+/mi';
                $re = '/^'.$word.'\r\n/mi';
                $found = preg_match_all($re, $this -> data, $matches);
                //file_put_contents('debug.log',$re.' -> '.$found."\r\n",FILE_APPEND );
                return $found;
            break;
            case 'array':
                //todo: implement
            break;
            case 'file':
                //todo: implement
            break;
        }
    }

    public function __destruct ()
    {
    }

    private function get_by_length ($which)
    {
        switch ( $this -> mode )
        {
            case 'string':
            default:
                switch ( $which )
                {
                    case 'longest':
                        function reduce($a, $b)
                        {
                            return strlen($a) > strlen($b) ? $a : $b;
                        }
                        return array_reduce(str_word_count($this -> data, 1), 'reduce');
                    break;
                    case 'shortest':
                        $words = str_word_count($this -> data, 1);
                        function cmp($a, $b) {
                            return strlen($b) - strlen($a);
                        }
                        usort($words, 'cmp');
                        return (array_pop($words));
                    break;
                }
            break;
            case 'array':
                //todo: implement
            break;
            case 'file':
                //todo: implement
            break;
        }
    }

    public function get_longest ($format='numeric')
    {
        $result = $this->get_by_length('longest');
        if ( $format == 'numeric' )
            return strlen($result);
        else
            return $result;
    }

    public function get_shortest ($format='numeric')
    {
        $result = $this->get_by_length('shortest');
        if ( $format == 'numeric' )
            return strlen($result);
        else
            return $result;
    }

}

class wordsearch
{

    private $letter_matrix;
    private $movement_rules;
    //private $results;
    private $dimensions = array ( 'x' => 0, 'y' => 0 );
    private $messages;
    private $words = array ();
    private $filters;

    public function __construct ( $letter_matrix, $movement_rules, $filters = array ( "minimum_length" => 3 ) )
    {
        $this -> letter_matrix = $letter_matrix;
        $this -> movement_rules = $movement_rules;
        end ( $this -> letter_matrix );
        $last_item_key = key ( $this -> letter_matrix );
        $dimensions = explode ( coordinate_separator, $last_item_key );
        $this -> dimensions [ 'x' ] = $dimensions [ 0 ];
        $this -> dimensions [ 'y' ] = $dimensions [ 1 ];
        $this -> messages .= 'matrix dimensions' . output_line_break . json_encode ( $this -> dimensions, JSON_PRETTY_PRINT ) . output_line_break;
        $this -> messages .= 'letter matrix' . output_line_break . json_encode ( $this -> letter_matrix, JSON_PRETTY_PRINT ) . output_line_break;
        $this -> messages .= 'movement rules' . output_line_break . json_encode ( $this -> movement_rules, JSON_PRETTY_PRINT ) . output_line_break;
        $this -> filters = $filters;
    }

    public function run ( $dictionary )
    {
        $this -> words = array ();
        for ( $x=0; $x <= $this -> dimensions [ 'x' ]; $x++ )
        {
            for ( $y=0; $y <= $this -> dimensions [ 'y' ]; $y++ )
            {
                foreach ( $this -> movement_rules as $movement_index => $movement_rule )
                {
                    $to_target = array ( 'path' => array (), 'word' => '' );
                    $this -> get_letters ( $x, $y, $to_target, $movement_rule, $dictionary );
                    //file_put_contents('debug.log',$to_target [ 'word' ].linux_line_break,FILE_APPEND);
                    //if ( strlen ( $to_target [ 'word' ] ) >= $dictionary -> shortest && strlen ( $to_target [ 'word' ] ) <= $dictionary -> longest )
                    if ( strlen ( $to_target [ 'word' ] ) >= $this -> filters [ 'minimum_length' ] && strlen ( $to_target [ 'word' ] ) >= $dictionary -> shortest && $dictionary -> gotcha ( $to_target [ 'word' ] ) == 1 && !$this -> path_exists ( $to_target [ 'path' ] ) )
                        $this -> words [] = $to_target;
                    unset ( $to_target );
                }
            }
        }
        $this -> messages .= 'words' . output_line_break . json_encode ( $this -> words, JSON_PRETTY_PRINT ) . output_line_break;
    }

    private function get_letters ( $from_x, $from_y, &$to_target, $rule, $dictionary )
    {
        $to_target [ 'word' ] .= $this -> get_letter ( $from_x, $from_y );
        $to_target [ 'path' ] [] = array ( 'x' => $from_x, 'y' => $from_y, 'rule_name' => $rule -> name );
        if ( strlen ( $to_target [ 'word' ] ) >= $this -> filters [ 'minimum_length' ] && strlen ( $to_target [ 'word' ] ) >= $dictionary -> shortest && $dictionary -> gotcha ( $to_target [ 'word' ] ) == 1 )
            $this -> words [] = $to_target;
        $next_x = $from_x + $rule -> move_x;
        $next_y = $from_y + $rule -> move_y;
        if ( $next_x < 0 || $next_x > $this -> dimensions [ 'x' ] || $next_y < 0 || $next_y > $this -> dimensions [ 'y' ] || strlen ( $to_target [ 'word' ] ) == $dictionary -> longest ) return;
        $to_target [ 'word' ] .= $this -> get_letters ( $next_x, $next_y, $to_target, $rule, $dictionary );
    }

    private function get_letter ( $x, $y )
    {
        $index = $x . coordinate_separator . $y;
        return $this -> letter_matrix [ $index ];
    }

    public function get_messages ( $stringify = true )
    {
        $result = array (
            array ( "title" => "matrix dimensions", "content" => &$this -> dimensions ),
            array ( "title" => "letter matrix", "content" => $this -> letter_matrix ),
            array ( "title" => "movement rules", "content" => $this -> movement_rules ),
            array ( "title" => "words", "content" => $this -> words )
        );
        if ( $stringify ) $result = json_encode ( $result, JSON_PRETTY_PRINT );
        return $result;
    }

    public function get_board ( $stringify = true )
    {
        $result = array ( 'dimensions' => $this -> dimensions, 'matrix' => $this -> letter_matrix );
        if ( $stringify )
            return json_encode ( $result, JSON_PRETTY_PRINT );
        else
            return $result;
    }

    public function get_solutions ( $stringify = true )
    {
        if ( $stringify )
            return json_encode ( $this -> words );
        else
            return $this -> words;
    }

    private function path_exists ( $path )
    {
        foreach ( array_reverse ( $this -> words ) as $word )
        {
            if ( $path == $word [ 'path' ] ) return true;
        }
        return false;
    }

    public function __destruct ()
    {
    }
}
