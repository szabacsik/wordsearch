<?php

//todo: error handling
$options = $_POST;
if ( !isset ( $options [ 'task' ] ) )
    $task = 'search';
else
{
    $task = $options ['task']; //render
    $options['board']['source'] = $options [ 'source' ];
}

//file_put_contents ( 'debug.log', var_export ( $options, true ) . linux_line_break, FILE_APPEND ); die ();

define ( 'dictionary_folder', dirname ( __FILE__ ) . '/docs/dictionaries/' );
define ( 'board_folder',  dirname ( __FILE__ ) . '/text/' );
$options['dictionary']['source'] = dictionary_folder . $options['dictionary']['source'] . '.txt';
$options['board']['source'] = board_folder . $options['board']['source'] . '.txt';

require_once ( 'inc/constants.php');
require_once ( 'inc/functions.php');
require_once ( 'inc/time.class.php');

$textfile_content = file_get_contents ( $options['board']['source'] );
$letter_matrix  = textfile_convert ( $textfile_content );
$movement_rules = json_decode ( file_get_contents ( dirname ( __FILE__ ) . '/src/movement_rules.json' ) );
$wordsearch = new wordsearch ( $letter_matrix, $movement_rules );

switch ( $task )
{
    case 'render':
        echo $wordsearch -> get_board ();
    break;
    case 'search':
    default:
        $time = new time ();
        $time_start = $time -> micro ();
        $time_start_human_readable = $time -> now ();

        $dictionary = new dictionary ( $options['dictionary']['source'], $options['dictionary']['method'] );

        $wordsearch -> run ( $dictionary );
        $time_end = $time -> micro ();
        $time_end_human_readable = $time -> now ();
        $execution_time = $time_end - $time_start;

        $messages = new stdClass ();
        $messages -> solutions = $wordsearch -> get_solutions ();
        $messages -> options = json_encode ( $options, JSON_PRETTY_PRINT );
        $messages -> wordsearch = $wordsearch -> get_messages ();
        $messages -> other[] = 'Task started at ' . $time_start_human_readable;
        $messages -> other[] = 'Task finished at ' . $time_end_human_readable . output_line_break . 'It took ' . $execution_time . ' seconds to complete';

        echo json_encode ( $messages, JSON_UNESCAPED_UNICODE );
    break;
}


